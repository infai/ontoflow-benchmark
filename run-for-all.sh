#!/bin/bash

source_dir=${1:?"No directory given"}
output_dir=public
log_file=log.csv

echo "ontology; count_triples; runtime; exitcode" > $log_file
nextflow pull https://gitlab.com/kupferdigital/ontoflow -r develop
for ontology_dir in $(find $source_dir -mindepth 1 -type d); do
  ontology_output_dir=${ontology_dir#"$source_dir"}
  ontology_output_dir=${output_dir}${ontology_output_dir:-/}
  ontology=$(echo ${ontology_dir}/*)
  echo
  echo $ontology
  start=`date +%s%N | cut -b1-13`
  nextflow run https://gitlab.com/kupferdigital/ontoflow -r develop -c develop.config --sourceDir $ontology_dir --outputDir $ontology_output_dir main.nf
  exitcode=$?
  end=`date +%s%N | cut -b1-13`
  runtime=$( echo "($end - $start) / 1000" | bc -l )
  count_triples=$(riot --count -q $ontology |& cat)
  count_triples_cut=${count_triples##* }
  echo $count_triples_cut
  echo $runtime
  echo "$ontology; $count_triples_cut; $runtime; $exitcode" >> $log_file
done

@prefix cc:    <http://creativecommons.org/ns#> .
@prefix dc11:  <http://purl.org/dc/elements/1.1/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix ns0:   <http://purl.org/vocab/vann/> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix ns1:   <http://www.w3.org/2003/06/sw-vocab-status/ns#> .
@prefix dc:    <http://purl.org/dc/terms/> .

<http://ns.inria.fr/semed/eduprogression/v1#hasKnowledge>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Relates an instance of EKS to an instance of Knowledge."@en ;
        rdfs:domain       <http://ns.inria.fr/semed/eduprogression/v1#EKS> ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has knowledge"@en ;
        rdfs:range        <http://ns.inria.fr/semed/eduprogression/v1#Knowledge> ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#LearningDomain>
        a                 owl:Class ;
        rdfs:comment      "A learning domain is represented by an instance of the LearningDomain class, and it is also an instance of skos:Concept that is part of (only) one skos:ConceptScheme containing the only learning domains of a progression. Also, as they are SKOS concepts, learning domains are hierarchically structured by using the skos:broader and/or skos:narrower properties. A learning domain can be associated to a Progression or to an EKS."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Learning domain"@en , "Domaine d'enseignement"@fr ;
        rdfs:subClassOf   owl:Thing ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#PointOfReference>
        a                 owl:Class ;
        rdfs:comment      "An instance of class PointOfReference represents an educational reference element on a specific element of knowledge and skills (an instance of EKS). An instance of EKS is related to an instance of class PointOfReference through the property hasPointOfReference."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Repère"@fr , "Point of reference"@en ;
        rdfs:subClassOf   owl:Thing ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#Skill>
        a                 owl:Class ;
        rdfs:comment      "It is represented by class Skill and described as well by using the Dublin Core and RDFS vocabularies. An instance of EKS is related to an instance of Skill through property hasSkill."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Skill"@en , "Competence"@fr ;
        rdfs:subClassOf   owl:Thing ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#Course>
        a                 owl:Class ;
        rdfs:comment      "In the French educational system, skills that students are expected to develop, are defined by cycle and each cycle is organized into course."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Course "@en , "Cours"@fr ;
        rdfs:subClassOf   owl:Thing ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#Progression>
        a                 owl:Class ;
        rdfs:comment      "A progression is represented by an instance of class Progression. It can be further described by using the Dublin Core or RDFS vocabularies. A progression can be associated to an existing learning domain (through the hasLearningDomain property) and to one or several EKSs (through the hasEKS property)."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Progression"@fr , "Progression"@en ;
        rdfs:subClassOf   owl:Thing ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#hasLearningDomain>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Associates a learning domain to a Progression or to an EKS."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has a learning sub domain"@en ;
        rdfs:range        <http://ns.inria.fr/semed/eduprogression/v1#LearningDomain> ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#hasVocabularyItem>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Relates an instance of EKS to an instance of the class VocabularyItem."@en ;
        rdfs:domain       <http://ns.inria.fr/semed/eduprogression/v1#EKS> ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has a vocabulary item"@en ;
        rdfs:range        <http://www.w3.org/2004/02/skos/core#Concept> ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#hasSkill>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Relates an instance of EKS to an instance of Skill."@en ;
        rdfs:domain       <http://ns.inria.fr/semed/eduprogression/v1#EKS> ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has skill"@en ;
        rdfs:range        <http://ns.inria.fr/semed/eduprogression/v1#Skill> ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#hasEKS>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Allows a progression to be associated to one or several EKSs."@en ;
        rdfs:domain       <http://ns.inria.fr/semed/eduprogression/v1#Progression> ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has an associated EKS "@en ;
        rdfs:range        <http://ns.inria.fr/semed/eduprogression/v1#EKS> ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#EKS>
        a                 owl:Class ;
        rdfs:comment      "An element of knowledge and skills is an instance of this class and it is associated to a set of knowledge pieces (class Knowledge) and skills (class Skill) for a specific French school cycle (class Course) containing reference points (class PointOfReference) and also a vocabulary of associated terms (class Vocabulary)."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Element of knowledge and skills "@en , "Element de connaissances et de competences"@fr ;
        rdfs:subClassOf   owl:Thing ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1>
        a                             owl:Ontology ;
        rdfs:comment                  "The EduProgression ontology formalizes the educational progressions of the French educational system, making possible to represent the existing progressions in a standard formal model, searchable and understandable by machines (OWL)."@en ;
        cc:license                    <http://creativecommons.org/licenses/by/3.0/> ;
        dc11:creator                  <https://plus.google.com/106595696646939943813> , <http://www.oscarrodriguez.me> ;
        dc11:description              "The EduProgression ontology formalizes the educational progressions of the French educational system, making possible to represent the existing progressions in a standard formal model, searchable and understandable by machines (OWL)."@en ;
        dc11:publisher                <http://dbpedia.org/resource/National_Institute_for_Research_in_Computer_Science_and_Control> ;
        dc11:rights                   "This ontology is distributed under a Creative Commons Attribution License - http://creativecommons.org/licenses/by/3.0 ."@en ;
        dc11:title                    "EduProgression Ontology"@en ;
        dc:issued                     "2016-09-01"^^<http://www.w3.org/2001/XMLSchema#string> ;
        dc:modified                   "2016-09-01"^^<http://www.w3.org/2001/XMLSchema#string> ;
        ns0:preferredNamespacePrefix  "eduprogression"^^<http://www.w3.org/2001/XMLSchema#string> ;
        ns0:preferredNamespaceUri     "http://ns.inria.fr/semed/eduprogression/v1#"^^<http://www.w3.org/2001/XMLSchema#string> .

<http://ns.inria.fr/semed/eduprogression/v1#Knowledge>
        a                 owl:Class ;
        rdfs:comment      "It is represented by class Knowledge and each of its instances is a skos:Concept that is part of a skos:ConceptScheme that contains all the knowledge pieces of a given progression. An instance of EKS is related to an instance of Knowledge through property hasKnowledge."@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Knowledge "@en , "Connaissance"@fr ;
        rdfs:subClassOf   owl:Thing ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#hasPointOfReference>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Relates an instance of EKS to an instance of class PointOfReference."@en ;
        rdfs:domain       <http://ns.inria.fr/semed/eduprogression/v1#EKS> ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has point of reference"@en ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#hasRelatedResource>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Associates an instance of a class with a resource on DBpedia"@en ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has realted resource on DBpedia"@en ;
        ns1:term_status   "stable" .

<http://ns.inria.fr/semed/eduprogression/v1#hasCourse>
        a                 owl:ObjectProperty ;
        rdfs:comment      "Relates an instance of EKS to an instance of Course."@en ;
        rdfs:domain       <http://ns.inria.fr/semed/eduprogression/v1#EKS> ;
        rdfs:isDefinedBy  <http://ns.inria.fr/semed/eduprogression#> ;
        rdfs:label        "Has course"@en ;
        rdfs:range        <http://ns.inria.fr/semed/eduprogression/v1#Course> ;
        ns1:term_status   "stable" .

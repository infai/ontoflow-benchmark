@prefix cc:    <http://creativecommons.org/ns#> .
@prefix schema: <http://schema.org/> .
@prefix gom:   <https://w3id.org/gom#> .
@prefix owl:   <http://www.w3.org/2002/07/owl#> .
@prefix omg:   <https://w3id.org/omg#> .
@prefix xsd:   <http://www.w3.org/2001/XMLSchema#> .
@prefix voaf:  <http://purl.org/vocommons/voaf#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .
@prefix wd:    <https://www.wikidata.org/entity/> .
@prefix geo:   <http://www.opengis.net/ont/geosparql#> .
@prefix dbp:   <http://dbpedia.org/property/> .
@prefix dbr:   <http://dbpedia.org/resource/> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix dcterms: <http://purl.org/dc/terms/> .
@prefix vann:  <http://purl.org/vocab/vann/> .
@prefix vs:    <http://www.w3.org/2003/06/sw-vocab-status/ns#> .
@prefix foaf:  <http://xmlns.com/foaf/0.1/> .
@prefix fog:   <https://w3id.org/fog#> .

gom:locallyTransformedByMatrix
        a                      owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment           "Property to link a omg:Geometry, that transforms (omg:transformsGeometry) another omg:Geometry (prototype geometry), to an RDF literal containing the 4x4 transformation matrix. {@en}x" ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "locally transformed by"@en ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry ;
        vs:term_status         "unstable" .

gom:hasTransformationMatrix
        a                     owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment          "has a single 4 X 4 transformation matrix. This datatype property links a gom:AffineCoordinateSystemTransformation to exactly one 4 X 4 transformation matrix M. Currently, two approaches can be used to define this matrix in one literal, i.e. using a JSON row major array (gom:rowMajorArray) or a column major array (gom:columnMajorArray)."@en ;
        rdfs:domain           gom:AffineCoordinateSystemTransformation ;
        rdfs:isDefinedBy      gom: ;
        rdfs:label            "has a transformation matrix"@en ;
        schema:rangeIncludes  gom:rowMajorArray , gom:columnMajorArray .

gom:columnMajorArray  a   rdfs:Datatype ;
        rdfs:comment      "a JSON column major array representing a square matrix. An example in Turtle of a literal representing a 2 X 2 matrix M: \"[m11,m21,m12,m22]\"^^gom:columnMajorArray"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "a column major array"@en .

gom:hasSurfaceArea  a          owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment           "the area of a volumetric (closed) geometry description. The units of this value are in m2"@en ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "has surface area"@en ;
        rdfs:range             xsd:decimal ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry .

gom:maxOcclusionDistance
        a                 owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The maximum calculated deviation distance to classify a point on the surface of the simplified geometry as being occluded. It is a setting of the represented accuracy analysis."@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "max occlusion distance (mm)"@en ;
        rdfs:range        xsd:decimal .

gom:Rhinoceros_v6  a      gom:GeometryModellingApplication ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Rhinoceros v6 CAD application"@en .

gom:hasFileSize  a             owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment           "the file size of the geometry description in bytes. If the geometry description includes material and/or texture files, the size of these files is included."@en ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "has file size"@en ;
        rdfs:range             xsd:nonNegativeInteger ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry .

gom:CoordinateSystemTransformation
        a                 owl:Class ;
        rdfs:comment      "A transformation between 3D Coordinate Systems. An instance of this class links to exactly two gom:CoordinateSystem instances using resp. gom:fromCoordinateSystem and gom:toCoordinateSystem. Each instance of gom:CoordinateSystemTransformation also links to one or multiple literals containing matrices or individual parameters defining the transformation."@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Coordinate System Transformation"@en .

<https://w3id.org/gom>
        a                              owl:Ontology , voaf:Vocabulary ;
        rdfs:comment                   "- Version 0.0.1: initial version"@en ;
        cc:license                     <https://creativecommons.org/licenses/by/4.0/> ;
        dcterms:creator                <https://www.researchgate.net/profile/Mathias_Bonduel> , <https://www.researchgate.net/profile/Pieter_Pauwels> , <https://www.researchgate.net/profile/Anna_Wagner13> ;
        dcterms:description            "The Geometry Metadata Ontology contains terminology to Coordinate Systems (CS), length units and other metadata (file size, software of origin, etc.). GOM is designed to be at least compatible with OMG (Ontology for Managing Geometry) and FOG (File Ontology for Geometry formats), and their related graph patterns.\n\nIn addition, GOM provides terminology for some experimental data structures to manage (marked as vs:term_status = unstable):\n* transformed geometry (e.g. a prototype door geometry that is reused for all doors of this type). This is closely related to the transformation of Coordinate Systems"@en ;
        dcterms:issued                 "2019-10-15"^^xsd:date ;
        dcterms:modified               "2019-10-15"^^xsd:date ;
        dcterms:title                  "GOM: Geometry Metadata Ontology"@en ;
        vann:example                   "https://raw.githubusercontent.com/mathib/gom-ontology/master/examples/gom-demo.json" , "https://raw.githubusercontent.com/mathib/fog-ontology/master/examples/sample_abox_snk_inspector.ttl" , "https://raw.githubusercontent.com/mathib/fog-ontology/master/examples/sample_abox_snk_contractor.ttl" ;
        vann:preferredNamespacePrefix  "gom" ;
        vann:preferredNamespaceUri     "https://w3id.org/gom#" ;
        owl:versionInfo                "0.0.1" .

gom:hasFaces  a                owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment           "the number of faces of a mesh geometry description"@en ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "has faces"@en ;
        rdfs:range             xsd:nonNegativeInteger ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry .

gom:hasSimplifiedGeometry
        a                     owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment          "The simplified geometry of a gom:RepresentedAccuracyAnalysis that is compared with a survey geometry."@en ;
        rdfs:domain           gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy      gom: ;
        rdfs:label            "has simplified geometry"@en ;
        schema:rangeIncludes  omg:GeometryState , omg:Geometry .

gom:createdIn  a               owl:FunctionalProperty , owl:ObjectProperty ;
        rdfs:comment           "property to link to the geometry modelling application the geometry description was created in"@en ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "created in"@en ;
        rdfs:range             gom:GeometryModellingApplication ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry .

gom:totalLOA10  a         owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The percentage of total relevant surface area of the simplified geometry that is occluded in LOA10 (deviation > 50 mm)"@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "% of total surface area in LOA10"@en ;
        rdfs:range        xsd:decimal .

gom:hasVertices  a             owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment           "the number of vertices of a mesh or point cloud geometry description"@en ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "has vertices"@en ;
        rdfs:range             xsd:nonNegativeInteger ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry .

gom:totalOccluded  a      owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The percentage of total relevant surface area of the simplified geometry that is occluded"@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "% occluded surface area"@en ;
        rdfs:range        xsd:decimal .

gom:fromCoordinateSystem
        a                 owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment      "links to an instance of gom:CoordinateSystem where the transformation results in"@en ;
        rdfs:domain       gom:CoordinateSystemTransformation ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "from Coordinate System"@en ;
        rdfs:range        gom:CoordinateSystem .

gom:totalLOA30  a         owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The percentage of total relevant surface area of the simplified geometry that is occluded in LOA30 (15 mm > deviation > 5 mm)"@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "% of total surface area in LOA30"@en ;
        rdfs:range        xsd:decimal .

gom:CartesianCoordinateSystem
        a                 owl:Class ;
        rdfs:comment      "A 3D Cartesian Coordinate System. One or multiple omg:Geometry or omg:GeometryState nodes can link to an instance of this class using gom:hasCoordinateSystem. If no named Coordinate System is linked explicitly to a geometry description, an unnamed Cartesian Coordinate System is assumed. A custom Cartesian Coordinate System can be registered in RDF to a world Cartesian Coordinate System by linking a gom:AffineCoordinateSystemTransformation instance to two instances of gom:CartesianCoordinateSystem"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Coordinate System"@en ;
        rdfs:subClassOf   gom:CoordinateSystem .

gom:PlanGeometry  a       owl:Class ;
        rdfs:comment      "A 2D plan geometry including floorplans, ceilingplans, elevations, sections and 2D ortographic drawings"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "2D plan geometry"@en ;
        rdfs:subClassOf   omg:Geometry .

gom:hasCoordinateSystem
        a                 owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment      "Geometry description is defined in exactly one Coordinate System. This property links a omg:Geometry of omg:GeometryState instance to a gom:CoordinateSystem instance."@en ;
        rdfs:domain       gom:CartesianCoordinateSystem ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "has length unit"@en ;
        rdfs:range        gom:LengthUnit .

gom:fromCartesianCoordinateSystem
        a                   owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment        "links to an instance of gom:CartesianCoordinateSystem where the transformation results in"@en ;
        rdfs:domain         gom:AffineCoordinateSystemTransformation ;
        rdfs:isDefinedBy    gom: ;
        rdfs:label          "from Cartesian Coordinate System"@en ;
        rdfs:range          gom:CartesianCoordinateSystem ;
        rdfs:subPropertyOf  gom:fromCoordinateSystem .

gom:totalLOA50  a         owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The percentage of total relevant surface area of the simplified geometry that is occluded in LOA50 (1 mm > deviation > 0 mm)"@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "% of total surface area in LOA50"@en ;
        rdfs:range        xsd:decimal .

gom:RepresentedAccuracyAnalysis
        a                 owl:Class ;
        rdfs:comment      "A 3D accuracy analysis executed to determine the represented LOA (Level Of Accuracy) as defined by the related USIBD specification. It is connected with exactly one source geometry resulting from a survey and one simplified 3D geometry."@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "represented accuracy analysis"@en .

gom:toCoordinateSystem
        a                 owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment      "links an instance of gom:CoordinateSystemTransformation to an instance of gom:CoordinateSystem where the transformation is applied to"@en ;
        rdfs:domain       gom:CoordinateSystemTransformation ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "from Coordinate System"@en ;
        rdfs:range        gom:CoordinateSystem .

gom:toCartesianCoordinateSystem
        a                   owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment        "links to an instance of gom:CartesianCoordinateSystem where the transformation is applied to"@en ;
        rdfs:domain         gom:AffineCoordinateSystemTransformation ;
        rdfs:isDefinedBy    gom: ;
        rdfs:label          "to Cartesian Coordinate System"@en ;
        rdfs:range          gom:CartesianCoordinateSystem ;
        rdfs:subPropertyOf  gom:toCoordinateSystem .

gom:MeshGeometry  a       owl:Class ;
        rdfs:comment      "A 3D mesh geometry consisting of planar faces connected via their edges"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Mesh geometry"@en ;
        rdfs:subClassOf   omg:Geometry .

gom:usedLocalModelMethod
        a                 owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "A boolean datatype property to express if the represented accuracy analysis method used a local model or not."@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "used local model method"@en ;
        rdfs:range        xsd:boolean .

gom:hasSurveyGeometry
        a                     owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment          "The survey geometry where a gom:RepresentedAccuracyAnalysis starts from."@en ;
        rdfs:domain           gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy      gom: ;
        rdfs:label            "has survey geometry"@en ;
        schema:rangeIncludes  omg:GeometryState , omg:Geometry .

gom:AffineCoordinateSystemTransformation
        a                 owl:Class ;
        rdfs:comment      "An affine transformation between 3D Cartesian Coordinate Systems. An instance of this class links to exactly two gom:CartesianCoordinateSystem instances using resp. gom:fromCartesianCoordinateSystem and gom:toCartesianCoordinateSystem. Each instance of gom:AffineCoordinateSystemTransformation also links to one or multiple literals containing matrices defining the transformation. In the simplest example, there is only one such linked literal containing an entire 4 X 4 transformation matrix, linked with the gom:hasTransformationMatrix datatype property."@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Affine Coordinate System Transformation"@en ;
        rdfs:subClassOf   gom:CoordinateSystemTransformation .

gom:totalRelevant  a      owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The percentage of total relevant surface area of the simplified geometry"@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "% of total relevant surface area"@en ;
        rdfs:range        xsd:decimal .

gom:hasVolume  a               owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment           "the volume of a volumetric (closed) geometry description. The units of this value are in m3"@en ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "has volume"@en ;
        rdfs:range             xsd:decimal ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry .

gom:PointCloudGeometry
        a                 owl:Class ;
        rdfs:comment      "A point cloud geometry consisting of a series of 3D points, with optional color and other scalar values"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "point cloud geometry"@en ;
        rdfs:subClassOf   omg:Geometry .

gom:GeometryModellingApplication
        a                 owl:Class ;
        rdfs:comment      "A geometry modelling application, used to create geometry descriptions"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "geometry modelling application"@en .

gom:hasEdges  a                owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment           "the number of edges of a mesh geometry description"@en ;
        rdfs:isDefinedBy       gom: ;
        rdfs:label             "has edges"@en ;
        rdfs:range             xsd:nonNegativeInteger ;
        schema:domainIncludes  omg:GeometryState , omg:Geometry .

gom:rowMajorArray  a      rdfs:Datatype ;
        rdfs:comment      "a JSON row major array representing a square matrix. An example in Turtle of a literal representing a 2 X 2 matrix M: \"[m11,m12,m21,m22]\"^^gom:rowMajorArray"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "a row major array"@en .

gom:LengthUnit  a         owl:Class ;
        rdfs:comment      "A length unit for a Cartesian Coordinate System. Each such Coordinate System has exactly one length unit (e.g. metre, kilometre, etc.). The instance of this class can be defined in QUDT or other ontologies for units."@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Length unit"@en .

gom:CoordinateSystem  a   owl:Class ;
        rdfs:comment      "A 3D Coordinate System. One or multiple omg:Geometry or omg:GeometryState nodes can link to an instance of this class using gom:hasCoordinateSystem. If no named Coordinate System is linked explicitly to a geometry description, an unnamed Cartesian Coordinate System is assumed. A custom Coordinate System can be registered in RDF by linking a gom:CoordinateSystemTransformation instance to this Coordinate System (gom:fromCoordinateSystem) and a second instance of gom:CoordinateSystem (gom:toCoordinateSystem)"@en ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Coordinate System"@en .

gom:total95PercentUpperLimit
        a                 owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The total deviation limit in mm where 95% of the surface area complies with."@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "total 95% deviation limit"@en ;
        rdfs:range        xsd:decimal .

gom:totalLOA20  a         owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The percentage of total relevant surface area of the simplified geometry that is occluded in LOA20 (50 mm > deviation > 15 mm)"@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "% of total surface area in LOA20"@en ;
        rdfs:range        xsd:decimal .

gom:hasLengthUnit  a      owl:ObjectProperty , owl:FunctionalProperty ;
        rdfs:comment      "Each Cartesian Coordinate System has exactly one length unit. If such a Coordinate System does not link to a length unit, it is assumed to be in metre."@en ;
        rdfs:domain       gom:CartesianCoordinateSystem ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "has length unit"@en ;
        rdfs:range        gom:LengthUnit .

gom:Meshlab_v2016  a      gom:GeometryModellingApplication ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "Meshlab v2016 application"@en .

gom:hasAnalysisFile  a    owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "Links to an RDF literal containing an embedded analysis file or a reference to such a file."@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "has analysis file"@en .

gom:totalLOA40  a         owl:FunctionalProperty , owl:DatatypeProperty ;
        rdfs:comment      "The percentage of total relevant surface area of the simplified geometry that is occluded in LOA40 (5 mm > deviation > 1 mm)"@en ;
        rdfs:domain       gom:RepresentedAccuracyAnalysis ;
        rdfs:isDefinedBy  gom: ;
        rdfs:label        "% of total surface area in LOA40"@en ;
        rdfs:range        xsd:decimal .

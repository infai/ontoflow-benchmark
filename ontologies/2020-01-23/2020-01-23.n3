@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix ns0: <http://purl.org/vocab/vann/> .
@prefix dc: <http://purl.org/dc/terms/> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix cc: <http://creativecommons.org/ns#> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix ns1: <http://www.w3.org/2003/06/sw-vocab-status/ns#> .
@prefix void: <http://rdfs.org/ns/void#> .
@prefix sd: <http://www.w3.org/ns/sparql-service-description#> .

<http://semanticturkey.uniroma2.it/ns/mdr>
  a owl:Ontology ;
  ns0:preferredNamespacePrefix "mdr" ;
  ns0:preferredNamespaceUri "http://semanticturkey.uniroma2.it/ns/mdr#" ;
  dc:title "The Semantic Turkey metadata registry ontology"@en ;
  dc:description """An application profile of DCAT combining it with other metadata vocabularies (e.g. VoID, DCTERMS, LIME)
 to meet requirements elicited in various use cases of the Semantic Web platform Semantic Turkey"""@en ;
  owl:versionInfo "1.0" ;
  owl:issued "2020-01-23"^^xsd:date ;
  dc:rights "Copyright Â© 2020 ART Group @ University of Rome Tor Vergata. All rights reserved." ;
  cc:license <https://spdx.org/licenses/BSD-3-Clause> ;
  dc:creator <http://art.uniroma2.it/fiorelli>, <http://art.uniroma2.it/stellato> ;
  owl:imports <http://www.w3.org/ns/dcat>, dc:, foaf:, <http://rdfs.org/ns/void>, <http://www.w3.org/ns/lemon/lime>, <http://www.w3.org/ns/sparql-service-description> .

<http://semanticturkey.uniroma2.it/ns/mdr#DereferenciationSystem>
  a owl:Class ;
  rdfs:label "dereferenciation system"@en ;
  rdfs:comment "a method to dereference the identifiers defined by some dataset"@en ;
  rdfs:isDefinedBy <http://semanticturkey.uniroma2.it/ns/mdr> ;
  ns1:term_status "stable" .

<http://semanticturkey.uniroma2.it/ns/mdr#SparqlEndpointLimitation>
  a owl:Class ;
  rdfs:label "sparql endpoint limitation"@en ;
  rdfs:comment "a limitation of some SPARQL endpoint"@en ;
  rdfs:isDefinedBy <http://semanticturkey.uniroma2.it/ns/mdr> ;
  ns1:term_status "stable" .

<http://semanticturkey.uniroma2.it/ns/mdr#dereferenciationSystem>
  a owl:ObjectProperty ;
  rdfs:label "dereferenciation system"@en ;
  rdfs:comment "relates a void:Dataset to a resource describing a method to dereference the identifiers defined by that dataset. Currently only two instances: :standardDereferenciation and :noDereferenciation are available, however this will be extended soon to account for versioning and other URI-transformation needs"@en ;
  rdfs:domain void:Dataset ;
  rdfs:range <http://semanticturkey.uniroma2.it/ns/mdr#DereferenciationSystem> ;
  rdfs:isDefinedBy <http://semanticturkey.uniroma2.it/ns/mdr> ;
  ns1:term_status "stable" .

<http://semanticturkey.uniroma2.it/ns/mdr#sparqlEndpointLimitation>
  a owl:ObjectProperty ;
  rdfs:label "sparql endpoint limitation"@en ;
  rdfs:comment "relates a SPARQL endpoint to a resource describing some limitation of that endpoint"@en ;
  rdfs:domain sd:Service ;
  rdfs:range <http://semanticturkey.uniroma2.it/ns/mdr#SparqlEndpointLimitation> ;
  rdfs:isDefinedBy <http://semanticturkey.uniroma2.it/ns/mdr> ;
  ns1:term_status "stable" .

<http://semanticturkey.uniroma2.it/ns/mdr#standardDereferenciation>
  a <http://semanticturkey.uniroma2.it/ns/mdr#DereferenciationSystem> ;
  rdfs:label "standard dereferenciation"@en ;
  rdfs:comment "the method to dereference identifiers by means of HTTP resolution"@en ;
  rdfs:isDefinedBy <http://semanticturkey.uniroma2.it/ns/mdr> ;
  ns1:term_status "stable" .

<http://semanticturkey.uniroma2.it/ns/mdr#noDereferenciation>
  a <http://semanticturkey.uniroma2.it/ns/mdr#DereferenciationSystem> ;
  rdfs:label "no dereferenciation"@en ;
  rdfs:comment "missing support for any dereferenciation method"@en ;
  rdfs:isDefinedBy <http://semanticturkey.uniroma2.it/ns/mdr> ;
  ns1:term_status "stable" .

<http://semanticturkey.uniroma2.it/ns/mdr#noAggregation>
  a <http://semanticturkey.uniroma2.it/ns/mdr#SparqlEndpointLimitation> ;
  rdfs:label "no aggregation"@en ;
  rdfs:comment "insufficient support for aggregation"@en ;
  rdfs:isDefinedBy <http://semanticturkey.uniroma2.it/ns/mdr> ;
  ns1:term_status "stable" .

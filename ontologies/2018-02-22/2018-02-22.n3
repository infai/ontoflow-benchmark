@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix : <http://www.w3.org/1999/xhtml> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix skos: <http://www.w3.org/2004/02/skos/core#> .
@prefix bibo: <http://purl.org/ontology/bibo/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .
@prefix vann: <http://purl.org/vocab/vann/> .
@prefix ibis: <https://privatealpha.com/ontology/ibis/1#> .

<http://danny.ayers.name/index.rdf#me>
    foaf:name "Danny Ayers"@en .

<http://doriantaylor.com/person/dorian-taylor#me>
    foaf:name "Dorian Taylor"@en .

<http://hyperdata.org/xmlns/ibis/>
    dct:creator <http://danny.ayers.name/index.rdf#me> .

<http://www.cc.gatech.edu/~ellendo/rittel/rittel-issues.pdf>
    dct:creator "Horst Rittel"@en, "Werner Kunz"@en ;
    dct:date "1970"@en ;
    dct:title "Issues as Elements of Information Systems"@en .

<http://www.cs.hut.fi/Opinnot/T-93.850/2005/Papers/gIBIS1988-conklin.pdf>
    dct:creator "Jeff Conklin"@en, "Michael L. Begeman"@en ;
    dct:date "1988"@en ;
    dct:title "gIBIS: a hypertext tool for exploratory policy discussion"@en .

<https://privatealpha.com/ontology/ibis/1>
    a bibo:Webpage .

<https://privatealpha.com/ontology/ibis/1#>
    dct:created "2012-12-11T22:22:53-08:00"^^xsd:dateTime ;
    dct:creator <http://doriantaylor.com/person/dorian-taylor#me> ;
    dct:modified "2012-12-12T16:04:50-08:00"^^xsd:dateTime, "2014-02-24T21:14:13Z"^^xsd:dateTime, "2018-02-22T03:39:14Z"^^xsd:dateTime ;
    dct:title "IBIS (bis) Vocabulary"@en ;
    bibo:uri <https://privatealpha.com/ontology/ibis/1#> ;
    vann:preferredNamespacePrefix "ibis"^^xsd:string ;
    a owl:Ontology ;
    rdfs:comment "This document specifies a vocabulary for describing an IBIS (issue-based information system)."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "IBIS"@en ;
    rdfs:seeAlso <http://dublincore.org/documents/dcmi-terms/>, <http://en.wikipedia.org/wiki/Issue-Based_Information_System>, <http://hyperdata.org/xmlns/ibis/>, <http://www.cc.gatech.edu/~ellendo/rittel/rittel-issues.pdf>, <http://www.cs.hut.fi/Opinnot/T-93.850/2005/Papers/gIBIS1988-conklin.pdf>, <http://www.w3.org/TR/prov-o/>, <https://privatealpha.com/ontology/ibis/1.n3>, <https://privatealpha.com/ontology/ibis/1.rdf>, <https://privatealpha.com/ontology/process-model/1#> ;
    owl:imports <http://www.w3.org/2004/02/skos/core#> ;
    owl:versionInfo "0.3"@en .

ibis:Argument
    a owl:Class ;
    rdfs:comment "An Argument is a type of Issue that explicitly supports or refutes a Position."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "Argument"@en ;
    rdfs:subClassOf ibis:Issue, [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Argument ;
        owl:onProperty ibis:replaces
    ], [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Argument ;
        owl:onProperty ibis:replaced-by
    ] ;
    owl:disjointWith ibis:Position ;
    skos:usageNote "An Argument need not only relate in scope to another Argument, but it must only be replaced by another argument."@en .

ibis:Invariant
    a owl:Class ;
    rdfs:comment "An Issue or Position can be marked Invariant to denote that it has been deemed outside of the influence of the Agents in the system, i.e., something to be steered around."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "Invariant"@en ;
    rdfs:subClassOf skos:Concept .

ibis:Issue
    a owl:Class ;
    rdfs:comment "An Issue is a state of affairs, claimed by one or more Agents to either be a misfit itself, or affecting some other Issue or Position."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "Issue"@en ;
    rdfs:subClassOf skos:Concept, [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Issue ;
        owl:onProperty skos:narrowerTransitive
    ], [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Issue ;
        owl:onProperty skos:broaderTransitive
    ], [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Issue ;
        owl:onProperty ibis:replaces
    ], [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Issue ;
        owl:onProperty ibis:replaced-by
    ] ;
    owl:disjointWith ibis:Position .

ibis:Network
    a owl:Class ;
    rdfs:comment "A network of issues, positions, and arguments."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "Network"@en ;
    rdfs:subClassOf skos:ConceptScheme .

ibis:Position
    a owl:Class ;
    rdfs:comment "A Position asserts a moral, ethical, pragmatic, or similar kind of assertion, typically identifying what, if anything, should be done about an Issue."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "Position"@en ;
    rdfs:subClassOf skos:Concept, [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Position ;
        owl:onProperty skos:broaderTransitive
    ], [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Position ;
        owl:onProperty ibis:replaces
    ], [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Position ;
        owl:onProperty ibis:replaced-by
    ], [
        a owl:Restriction ;
        owl:allValuesFrom ibis:Position ;
        owl:onProperty skos:narrowerTransitive
    ] ;
    owl:disjointWith ibis:Argument, ibis:Issue .

ibis:concerns
    a owl:ObjectProperty ;
    rdfs:comment "The subject is an issue concerning the object, which can be any resource."@en ;
    rdfs:domain ibis:Issue ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "concerns"@en ;
    rdfs:range owl:Thing .

ibis:endorsed-by
    a owl:ObjectProperty ;
    rdfs:comment "A concept can be endorsed by an Agent without said Agent having mentioned or advanced it initially, and without any additional comment."@en ;
    rdfs:domain skos:Concept ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "endorsed by"@en ;
    rdfs:range foaf:Agent ;
    owl:inverseOf ibis:endorses ;
    skos:note "This term, along with ibis:endorses, enables an Agent to signal its agreement with a concept. To signal disagreement, explain why with an ibis:Argument that ibis:opposes the concept."@en .

ibis:endorses
    a owl:ObjectProperty ;
    rdfs:comment "An Agent can endorse a concept without having initially mentioned or advanced it, and without any additional comment."@en ;
    rdfs:domain foaf:Agent ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "endorses"@en ;
    rdfs:range skos:Concept ;
    owl:inverseOf ibis:endorsed-by ;
    skos:note "This term, along with ibis:endorsed-by, enables an Agent to signal its agreement with a concept. To signal disagreement, explain why with an ibis:Argument that ibis:opposes the concept."@en .

ibis:generalizes
    a owl:ObjectProperty ;
    rdfs:comment "The subject is a more generic form of the object."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "generalizes"@en ;
    owl:equivalentProperty skos:narrower ;
    owl:inverseOf ibis:specializes ;
    skos:note "The equivalent property skos:narrower asserts that the object is narrower than the subject, while the subject of ibis:generalizes is more general than the object."@en .

ibis:opposed-by
    a owl:ObjectProperty ;
    rdfs:comment "Indicates a subject position opposed by an object argument."@en ;
    rdfs:domain ibis:Position ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "opposed by"@en ;
    rdfs:range ibis:Argument ;
    owl:inverseOf ibis:opposes .

ibis:opposes
    a owl:ObjectProperty ;
    rdfs:comment "Indicates a subject argument that opposes an object position."@en ;
    rdfs:domain ibis:Argument ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "opposes"@en ;
    rdfs:range ibis:Position ;
    owl:inverseOf ibis:opposed-by .

ibis:questioned-by
    a owl:ObjectProperty ;
    rdfs:comment "Indicates a belief called into question by an issue."@en ;
    rdfs:domain skos:Concept ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "questioned by"@en ;
    rdfs:range ibis:Issue ;
    owl:inverseOf ibis:questions .

ibis:questions
    a owl:ObjectProperty ;
    rdfs:comment "Indicates an issue that raises doubt on a belief."@en ;
    rdfs:domain ibis:Issue ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "questions"@en ;
    rdfs:range skos:Concept ;
    owl:inverseOf ibis:questioned-by .

ibis:replaced-by
    a owl:ObjectProperty ;
    rdfs:comment "Indicates when a concept is replaced by another concept of the same type."@en ;
    rdfs:domain skos:Concept ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "replaced by"@en ;
    rdfs:range skos:Concept ;
    rdfs:subPropertyOf dct:isReplacedBy ;
    owl:inverseOf ibis:replaces .

ibis:replaces
    a owl:ObjectProperty ;
    rdfs:comment "Indicates when a concept replaces another concept of the same type."@en ;
    rdfs:domain skos:Concept ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "replaces"@en ;
    rdfs:range skos:Concept ;
    rdfs:subPropertyOf dct:replaces ;
    owl:inverseOf ibis:replaced-by .

ibis:responds-to
    a owl:ObjectProperty ;
    rdfs:comment "Indicates an issue to which the subject position responds."@en ;
    rdfs:domain ibis:Position ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "responds to"@en ;
    rdfs:range ibis:Issue ;
    owl:inverseOf ibis:response .

ibis:response
    a owl:ObjectProperty ;
    rdfs:comment "Indicates a position that responds to the subject issue."@en ;
    rdfs:domain ibis:Issue ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "response"@en ;
    rdfs:range ibis:Position ;
    owl:inverseOf ibis:responds-to .

ibis:specializes
    a owl:ObjectProperty ;
    rdfs:comment "The subject is a more specific form of the object."@en ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "specializes"@en ;
    owl:equivalentProperty skos:broader ;
    owl:inverseOf ibis:generalizes ;
    skos:note "The equivalent property skos:broader asserts that the object is broader than the subject, while the subject of ibis:specializes is more specific than the object."@en .

ibis:suggested-by
    a owl:ObjectProperty ;
    rdfs:comment "Indicates when the subject issue is suggested by the object belief."@en ;
    rdfs:domain ibis:Issue ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "suggested by"@en ;
    rdfs:range skos:Concept ;
    owl:inverseOf ibis:suggests .

ibis:suggests
    a owl:ObjectProperty ;
    rdfs:comment "Indicates when the subject belief suggests the object issue."@en ;
    rdfs:domain skos:Concept ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "suggests"@en ;
    rdfs:range ibis:Issue ;
    owl:inverseOf ibis:suggested-by .

ibis:supported-by
    a owl:ObjectProperty ;
    rdfs:comment "Indicates a subject position supported by an object argument."@en ;
    rdfs:domain ibis:Position ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "supported by"@en ;
    rdfs:range ibis:Argument ;
    owl:inverseOf ibis:supports .

ibis:supports
    a owl:ObjectProperty ;
    rdfs:comment "Indicates a subject argument that supports an object position."@en ;
    rdfs:domain ibis:Argument ;
    rdfs:isDefinedBy <https://privatealpha.com/ontology/ibis/1#> ;
    rdfs:label "supports"@en ;
    rdfs:range ibis:Position ;
    owl:inverseOf ibis:supported-by .

